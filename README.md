# Sudoku solver

A quick sudoku solver I wrote while on holiday for something to do. May not be able to solve all sudoku puzzles - I haven't tried many yet.

Each square starts out with all options being possible and they get removed as the algorithms solve the puzzle.

## Algorithms used

- update_options(): Update the possible options for all unsolved cells (i.e. remove options that match a solved number in the same row, col or 3x3 square)
- check_solved(): Find unsolved squares with only one solution and mark them as solved
- Per-group algorithms - these operate on a single row, column or 3x3 square at a time:
    - check_group_for_uniques(): Check a group to see if any numbers are only an option in one square. If they are, that square is solved
    - check_group_for_subgroups(): Check a group to see if any subgroups have the same number of possibilities as the number of cells in the subgroup. If there are, those numbers can be removed from the other cells.
