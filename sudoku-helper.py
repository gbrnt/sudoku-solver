"""
Sudoku helper
"""

from itertools import combinations


class Cell:
    """
    A single 1x1 cell of the Sudoku puzzle - not a 3x3 square
    """

    def __init__(self, number=None):
        # Make importing easier by changing space to None
        if not number.isdigit():
            self.number = None
        else:
            self.number = int(number)

        if self.number is not None:
            # Solution already found
            self.is_solved = True
            self.options = set([self.number])
        else:
            # Solution for this cell not found yet - assume all are possible to start with
            self.is_solved = False
            self.options = set([1, 2, 3, 4, 5, 6, 7, 8, 9])

    def __str__(self):
        if self.is_solved:
            return str(self.number)
        else:
            return "_"

    def __repr__(self):
        if self.is_solved:
            return str(self.number)
        else:
            return "_"

    def __int__(self):
        """
        Convert to an integer for comparison
        If number is not known it returns 0, as this is never a valid option
        """
        if self.is_solved:
            return self.number
        else:
            return 0

    def remove_options(self, options):
        """
        Remove a set of numbers from the possible options
        """

        self.options -= options


class Sudoku:
    """
    A full 9x9 Sudoku puzzle
    """

    def __init__(self, sudoku_list):
        self.grid = [[Cell(cell) for cell in row] for row in sudoku_list]
        self.update_options()
        print("Starting grid:")
        print(self)

        # See if any are already solved to start with (only one option)
        self.check_solved()
        print("Including already solved:")
        print(self)

        last_stats = 0
        current_stats = self.get_stats()
        iteration = 1
        while last_stats != current_stats:
            # Go through all our solution algorithms for all groups
            self.check_all_groups()
            self.check_solved()
            self.update_options()
            self.check_solved()
            print(f"\nIteration {iteration}:")
            print(self)

            # Update variables for next iteration of the while loop
            last_stats = current_stats
            current_stats = self.get_stats()
            iteration += 1

            # If all squares are solved, we're done!
            if current_stats[0] == 81:
                print("Sudoku solved!")
                break
        else:
            print("Solution is no longer changing - these algorithms cannot solve it")
            print("Grid with all options:")
            print(self.str_with_all_options())

    def __str__(self, print_all_options=False):
        """
        For printing - show puzzle as a full grid
        Solved cells are shown in square brackets
        Unsolved cells just have the number of options shown
        If print_all_options is true, prints the options instead of the number of them
        """
        rows = []
        for row in self.grid:
            rowrepr = []
            for cell in row:
                if cell.is_solved:
                    rowrepr.append(f"[{cell.number}]")
                else:
                    if not print_all_options:
                        rowrepr.append(f" {len(cell.options)} ")
                    else:
                        options_joined = "/".join(str(o) for o in cell.options)
                        rowrepr.append(f" {options_joined} ")
            rows.append(" ".join(rowrepr))
        (
            total_solved,
            total_unsolved,
            solved_percentage,
            total_options,
        ) = self.get_stats()
        solved_message = f"\n{total_solved} solved ({solved_percentage}%), {total_unsolved} unsolved - {total_options} options remaining"
        return "\n".join(rows) + solved_message

    def str_with_all_options(self):
        return self.__str__(print_all_options=True)

    def get_stats(self):
        """
        Return total solved, total unsolved, solved percentage and total options remaining
        """
        total_solved = self.count_solved()
        total_unsolved = 81 - total_solved
        solved_percentage = int(total_solved / 81 * 100)
        total_options = self.count_options()

        return total_solved, total_unsolved, solved_percentage, total_options

    def count_solved(self):
        """
        Return the number of solved cells
        """
        flattened = [cell for row in self.grid for cell in row if cell.is_solved]
        return len(flattened)

    def count_options(self):
        """
        Return the total number of options in unsolved cells
        This is useful to see if algorithms get closer to the solution
        (even if they don't actually solve any cells)
        """
        options_per_cell = [
            len(cell.options) for row in self.grid for cell in row if not cell.is_solved
        ]
        return sum(options_per_cell)

    def row(self, rownum):
        """
        Return a single row of the puzzle (row number 1-9)
        """
        return self.grid[rownum - 1]

    def col(self, colnum):
        """
        Return a single column of the puzzle (col number 1-9)
        """
        return [row[colnum - 1] for row in self.grid]

    def square(self, squarenum):
        """
        Get the numbers for a specific square of the sudoku:
        1 2 3
        4 5 6
        7 8 9
        """

        # Row set and col set define which rows and cols make the square
        row_set = ((squarenum - 1) // 3) * 3
        col_set = ((squarenum + 2) % 3) * 3

        square = [
            cell
            for row in self.grid[row_set : row_set + 3]
            for cell in row[col_set : col_set + 3]
        ]

        return square

    def cell(self, rownum, colnum):
        """
        Get a single cell from the grid
        """
        return self.grid[rownum - 1][colnum - 1]

    def squarenum(self, rownum, colnum):
        """
        Get the number of the square from the rownum and colnum
        """
        rowgroup = (rownum - 1) // 3
        colgroup = (colnum - 1) // 3
        return rowgroup * 3 + colgroup + 1

    def update_options(self):
        """
        Update the possible options for all the unsolved cells
        """

        for rownum in range(1, 10):
            for colnum in range(1, 10):
                cell = self.cell(rownum, colnum)

                # First, if it's already solved, skip it:
                if cell.is_solved:
                    continue

                # First, find the set of numbers it can't be:
                row = set([int(cell) for cell in self.row(rownum)])
                col = set([int(cell) for cell in self.col(colnum)])
                square = set(
                    [int(cell) for cell in self.square(self.squarenum(rownum, colnum))]
                )
                impossible_options = row.union(col, square)

                # Then subtract those from the possible options
                cell.remove_options(impossible_options)

    def check_solved(self):
        """
        Find any unsolved squares with only one solution and mark them as solved
        """

        for row in self.grid:
            for cell in row:
                if not cell.is_solved and len(cell.options) == 1:
                    cell.number = cell.options.pop()  # This removes it from the set
                    cell.options.add(cell.number)  # So let's re-add it
                    cell.is_solved = True

    def check_all_groups(self):
        """
        Check all groups (rows, columns, squares) in every way we can
        """
        check_functions = [
            self.check_group_for_uniques,
            self.check_group_for_subgroups,
        ]

        for func in check_functions:
            for i in range(1, 10):
                func(self.row(i))
                func(self.col(i))
                func(self.square(i))

    def check_group_for_uniques(self, group):
        """
        Check a group (row, column or square) to see if any numbers are only
        an option in one square. If they are, that cell is solved.
        """

        numbers = {
            1: [],
            2: [],
            3: [],
            4: [],
            5: [],
            6: [],
            7: [],
            8: [],
            9: [],
        }

        for cell in group:
            for number in cell.options:
                numbers[number].append(cell)

        for number, cells in numbers.items():
            if len(cells) == 1:
                cell = cells[0]
                if not cell.is_solved:
                    # Set cell to solved
                    cell.number = number
                    cell.options = {number}
                    cell.is_solved = True

                    # Found a unique one, so we should call the function again
                    # to see if any others have now become unique
                    # Then we can exit this loop early as it's been done in the
                    # called iteration of the function
                    self.check_group_for_uniques(group)
                    break

    def check_group_for_subgroups(self, group):
        """
        Check a group (row, column or square) to see if any subgroups have the
        same number of possibilities as the number of cells in the subgroup.
        If there are, those numbers can be removed from the other cells.
        """

        subgroup_sizes = [2, 3, 4]

        unsolved_cells = [cell for cell in group if not cell.is_solved]
        for size in subgroup_sizes:
            for subgroup in combinations(unsolved_cells, size):
                options = [0] * 10  # 10 elements, so we can use 1-indexing
                options_set = set()
                for cell in subgroup:
                    for option in cell.options:
                        options[option] += 1
                        options_set.add(option)

                # The number of non-zero options is the number of options in subgroup
                num_options = len([n for n in options if n != 0])

                # If we have the same number of options as cells in this subgroup
                if len(subgroup) == num_options:
                    # Remove those numbers from the other cells in group!
                    # print(list(cell.options for cell in subgroup))
                    # print(options_set)
                    for cell in group:
                        if not cell in subgroup:
                            cell.remove_options(options_set)


def help_sudoku():
    """
    Ask for the details of the sudoku and find hints to help solve it
    (Or just find a solution)
    """
    sudoku_input = get_input_sudoku()
    # sudoku_input = dummy_sudoku()

    sudoku = Sudoku(sudoku_input)


def get_input_sudoku():
    """
    Get an input sudoku challenge from the user
    """

    print("Enter a sudoku line by line - use space for unknown squares")
    sudoku = [list(input(f"Line {l} > ")) for l in range(9)]
    return sudoku


def dummy_sudoku():
    """
    Create a dummy sudoku challenge for testing
    """
    sudoku_input = [
        list("   43   1"),
        list("   85  9 "),
        list("  9  743 "),
        list("  19 3 2 "),
        list("  8   6  "),
        list(" 7 2 43  "),
        list(" 923  7  "),
        list(" 5  98   "),
        list("6        "),
    ]
    return sudoku_input


if __name__ == "__main__":
    help_sudoku()
